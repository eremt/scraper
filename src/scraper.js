const fetch = require('isomorphic-fetch')
const cheerio = require('cheerio')
const { isObjLiteral } = require('./utils.js')

class Scraper {
  fetch (_urls, schema = null) {
    // array of urls to fetch
    const urls = typeof _urls === 'string' ? [_urls]  : _urls

    // map urls to promises, timeout is used to fix timeout issue - research why this works
    const promises = urls.map((href, index) => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          fetch(href)
            .then(r => r.text())
            .then(body => {
              return resolve({
                location: this.location(href),
                body
              })
            })
            .catch(error => reject(error))
        }, index * 25)
      })
    })

    // when promises resolve, build the return object
    return Promise.all(promises)
      .then(responses => {
        return responses.map(response => {
          const { body } = response
          const $ = cheerio.load(body, { decodeEntities: false })

          let result = {
            ...response,
            og: this.og($),
            body,
          }
          
          if (schema) {
            result = {
              ...result,
              data: this.parse($, null, schema)
            }
          }
          
          return result
        })
      })
      .catch(error => {
        console.log('Error in Scraper.fetch', error)
      })
  }

  location (href, location = null) {
    if (location && href.search(location.href) === -1) {
      if (href.search(location.host) === -1) {
        return {
          ...location,
          href: location.protocol + '//' + location.host + href
        }
      }
    }
    /**
     * TODO:
     * Match any accepted character such as ÅÄÖ etc
     * Research valid url characters
     */
    const regex = /((https?:)?(?:\/\/)?([\w.-]+))(\/?[\w-?=&%\/ ]+)?/
    const [
      match,
      origin,
      protocol,
      host,
      pathname
    ] = regex.exec(href)

    return {
      host,
      href,
      origin,
      pathname,
      protocol
    }
  }

  og ($) {
    const result = {}
    $('meta[property^="og:"]').map((i, el) => {
      let [ tmp, ...key ] = $(el).attr('property').split(':')
      key = key.join(':')
      const value = $(el).attr('content')
      result[key] = value
    })
    return result
  }
  
  parse ($, context, schema) {
    if (!schema) {
      return null
    }
    if (typeof schema === 'function') {
      return schema($, context)
    }    
    
    return Object.entries(schema).reduce((o, [key, value]) => {
      let type = typeof value
      if (Array.isArray(value)) {
        type = 'array' // this will probably be used later, just need to figure out a good implementation
      }

      switch (type) {    
        // Type string, if matches are more than 1 return an array with the text of each, else just return the string
        case 'string':
          const v = $(value)
          o[key] = v.length === 1 ? v.text() : v.map((i, e) => $(e).text()).get()
          break
        
        // Type function return the callback with cheerio and context
        case 'function':
          o[key] = value($, context)
          break
        
        // Type object, return recursive this.parse
        case 'object':
          o[key] = this.parse($, context, value)
          break
        
        default:
          console.log('Scraper.parse: Unhandled type', type)
          break
      }

      return o
    }, {})
  }
}

module.exports = new Scraper()

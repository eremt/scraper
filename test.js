const scraper = require('./index.js')

scraper.fetch('https://gitlab.com/eremt/scraper').then(response => {
  response[0].body = null // clean up response output for easier debugging
  console.log(JSON.stringify(response, null, 2))
})

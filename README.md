## Installation
```bash
$ npm install https://gitlab.com/eremt/scraper.git
```
## Usage
```js
const scraper = require('scraper')

scraper.fetch('https://gitlab.com/eremt/scraper')
  .then(response => console.log(response))
// [
//   {
//     "location": {
//       "host": "gitlab.com",
//       "href": "https://gitlab.com/eremt/scraper",
//       "origin": "https://gitlab.com",
//       "pathname": "/eremt/scraper",
//       "protocol": "https:"
//     },
//     "body": "<!DOCTYPE html> ... ",
//     "og": {
//       "type": "object",
//       "site_name": "GitLab",
//       "title": "Troy Erem / scraper",
//       "description": "GitLab.com",
//       "image": "https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png",
//       "image:width": "64",
//       "image:height": "64",
//       "url": "https://gitlab.com/eremt/scraper"
//     }
//   }
// ]
```
